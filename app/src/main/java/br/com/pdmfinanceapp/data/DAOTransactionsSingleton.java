package br.com.pdmfinanceapp.data;

import android.content.Context;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

import br.com.pdmfinanceapp.model.Transaction;

public class DAOTransactionsSingleton {

    private ArrayList<Transaction> transactions;
    private static DAOTransactionsSingleton INSTANCE;

    private DAOTransactionsSingleton() {

    }

    public ArrayList<Transaction> getTransactions() {
        if (this.transactions == null) {
            this.transactions = new ArrayList<>();
        }
        return this.transactions;
    }

    public void addTransaction(Transaction transaction) {
        if (this.transactions == null) {
            this.transactions = new ArrayList<>();
        }
        this.transactions.add(transaction);
    }

    public static DAOTransactionsSingleton getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new DAOTransactionsSingleton();
        }
        return INSTANCE;
    }

    public void exportTransactions(Context context) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyMMddHHmmss");
        String FILENAME = dtf.format(LocalDateTime.now());
        File file = new File(context.getFilesDir(), FILENAME);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            String fileString = this.transactions.stream().map(transaction -> transaction.toCsvString()).reduce("", String::concat);
            writer.write(fileString);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
