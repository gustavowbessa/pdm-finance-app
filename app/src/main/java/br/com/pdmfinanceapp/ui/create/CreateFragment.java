package br.com.pdmfinanceapp.ui.create;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import br.com.pdmfinanceapp.R;
import br.com.pdmfinanceapp.constants.TransactionType;
import br.com.pdmfinanceapp.data.DAOTransactionsSingleton;
import br.com.pdmfinanceapp.model.Transaction;

public class CreateFragment extends Fragment {

    private TextView transactionDescription;
    private TextView transactionValue;
    private Spinner transactionType;
    private Button btnSave;

    private TransactionType type;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_create, container, false);
        type = TransactionType.CREDIT;

        transactionDescription = root.findViewById(R.id.transaction_desc);
        transactionValue = root.findViewById(R.id.transaction_price);
        transactionType = root.findViewById(R.id.spinner);
        transactionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String description = transactionType.getSelectedItem().toString();
                type = TransactionType.getByDescription(description);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                System.out.println("Nada escolhido ...");
            }
        });

        btnSave = root.findViewById(R.id.button);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTransaction();
            }
        });

        return root;
    }

    public void saveTransaction() {
        String description = transactionDescription.getText().toString();
        Double value = Double.valueOf(transactionValue.getText().toString());

        Transaction transaction = new Transaction();
        transaction.setDescription(description);
        transaction.setValue(value);
        transaction.setType(type);

        DAOTransactionsSingleton.getINSTANCE().addTransaction(transaction);

        transactionDescription.setText("");
        transactionValue.setText("");
        transactionType.setSelection(0);
        Toast toast = Toast.makeText(getContext(), "Transação salva.", Toast.LENGTH_SHORT);
        toast.show();
    }
}