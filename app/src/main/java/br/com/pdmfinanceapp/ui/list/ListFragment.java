package br.com.pdmfinanceapp.ui.list;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import br.com.pdmfinanceapp.MainActivity;
import br.com.pdmfinanceapp.R;
import br.com.pdmfinanceapp.constants.TransactionType;
import br.com.pdmfinanceapp.data.DAOTransactionsSingleton;
import br.com.pdmfinanceapp.list_builders.TransactionListBuilder;
import br.com.pdmfinanceapp.model.Transaction;

public class ListFragment extends Fragment {

    private TextView txtTransactionTotal;
    private FloatingActionButton fab;
    AlertDialog alertDialog;

    public ListFragment() {
        super(R.layout.fragment_list);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        txtTransactionTotal = root.findViewById(R.id.transaction_total);
        fab = root.findViewById(R.id.floatingActionButton);

        alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setTitle("Relatório Financeiro:");
        alertDialog.setMessage("Você deseja emitir um relatório financeiro?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SIM",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DAOTransactionsSingleton.getINSTANCE().exportTransactions(getContext());
                        Toast toast = Toast.makeText(getContext(), "Relatório financeiro foi emitido.", Toast.LENGTH_SHORT);
                        toast.show();
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NÃO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        txtTransactionTotal.setText(getTransactionTotalValue());
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.show();
            }
        });

        if (!DAOTransactionsSingleton.getINSTANCE().getTransactions().isEmpty()) {
            fab.setVisibility(View.VISIBLE);
        }

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<Transaction> transactions = DAOTransactionsSingleton.getINSTANCE().getTransactions();
        new TransactionListBuilder(getActivity(), R.id.transaction_list, view)
                .load(transactions, getActivity());
    }

    private String getTransactionTotalValue() {
        NumberFormat df = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        Double totalValue = DAOTransactionsSingleton.getINSTANCE().getTransactions().stream().map(
                transaction -> transaction.getType().equals(TransactionType.CREDIT) ? transaction.getValue() : transaction.getValue() * -1D
        ).reduce(0D, Double::sum);
        return df.format(totalValue);
    }
}