package br.com.pdmfinanceapp.list.transaction;

import android.app.Activity;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.com.pdmfinanceapp.R;
import br.com.pdmfinanceapp.model.Transaction;

public class TransactionDataAdapter extends RecyclerView.Adapter<TransactionDataViewHolder> {

    private ArrayList<Transaction> transactions;
    private SparseBooleanArray toggleInfo;

    public TransactionDataAdapter(ArrayList<Transaction> transactions, Activity activity) {
        this.transactions = transactions;
        this.toggleInfo = new SparseBooleanArray();
    }

    @NonNull
    @Override
    public TransactionDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.transaction_item_view, parent, false);
        return new TransactionDataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionDataViewHolder holder, int position) {
        holder.bind(this.transactions.get(position));
    }

    @Override
    public int getItemCount() {
        return this.transactions.size();
    }

    //----------- Métodos auxiliares

    public void setOpenViewCacheFor(int id) {
        this.toggleInfo.put(id, true);
    }

    public void unsetOpenViewCacheFor(int id) {
        this.toggleInfo.delete(id);
    }

    public boolean getOpenViewCacheFor(int id) {
        return this.toggleInfo.get(id, false);
    }
}
