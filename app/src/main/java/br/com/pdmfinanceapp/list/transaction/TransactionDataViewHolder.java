package br.com.pdmfinanceapp.list.transaction;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.pdmfinanceapp.R;
import br.com.pdmfinanceapp.constants.TransactionType;
import br.com.pdmfinanceapp.model.Transaction;

public class TransactionDataViewHolder extends RecyclerView.ViewHolder {

    private final TextView txtTransactionDescription;
    private final TextView txtTransactionPrice;
    private final View viewTransactionType;

    public TransactionDataViewHolder(@NonNull View itemView) {
        super(itemView);
        this.txtTransactionDescription = itemView.findViewById(R.id.transaction_desc_label);
        this.txtTransactionPrice = itemView.findViewById(R.id.transaction_price_label);
        this.viewTransactionType = itemView.findViewById(R.id.transaction_type_label);
    }

    public void bind(Transaction transaction) {
        this.txtTransactionDescription.setText(transaction.getDescription());
        this.txtTransactionPrice.setText(transaction.getFormattedValue());
        int bgColor = transaction.getType().equals(TransactionType.CREDIT) ? R.color.credit : R.color.debit;
        this.viewTransactionType.setBackgroundResource(bgColor);
    }

}
