package br.com.pdmfinanceapp.list_builders;

import android.app.Activity;
import android.view.View;

import androidx.annotation.IdRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.com.pdmfinanceapp.list.transaction.TransactionDataAdapter;
import br.com.pdmfinanceapp.model.Transaction;

public class TransactionListBuilder {
    private RecyclerView rvTransactionList;
    private LinearLayoutManager layoutManager;
    private TransactionDataAdapter adapter;

    public TransactionListBuilder(Activity activity, @IdRes int rvTransactions, View view) {
        this.rvTransactionList = view.findViewById(rvTransactions);
        this.layoutManager = new LinearLayoutManager(activity);
        this.rvTransactionList.setLayoutManager(this.layoutManager);
        this.adapter = null;
    }

    public TransactionListBuilder load(ArrayList<Transaction> transactions, Activity activity) {
        this.adapter = new TransactionDataAdapter(transactions, activity);
        this.rvTransactionList.setAdapter(this.adapter);
        return this;
    }

}
