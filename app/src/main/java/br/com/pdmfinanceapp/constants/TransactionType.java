package br.com.pdmfinanceapp.constants;

public enum TransactionType {
    CREDIT("Crédito"), DEBIT("Débito");
    private String descricao;

    TransactionType(String descricao) {
        this.descricao = descricao;
    }

    public static TransactionType getByDescription(String description) {
        for (TransactionType transaction : TransactionType.values()) {
            if(transaction.getDescricao().equals(description) == true) {
                return transaction;
            }
        }
        return null;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
