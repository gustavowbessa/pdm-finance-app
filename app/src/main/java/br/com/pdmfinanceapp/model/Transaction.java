package br.com.pdmfinanceapp.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import br.com.pdmfinanceapp.constants.TransactionType;

public class Transaction {

    private Integer id;
    private Double value;
    private TransactionType type;
    private String description;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFormattedValue() {
        String formattedValue = "";
        NumberFormat df = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        if (this.type.equals(TransactionType.DEBIT)) {
            formattedValue = formattedValue.concat("-");
        }
        formattedValue = formattedValue.concat(df.format(new BigDecimal(this.value)));
        return formattedValue;
    }

    public String toCsvString() {
        Double transactionValue = this.type.equals(TransactionType.CREDIT) ? this.value : this.value * -1;
        String csvString = this.description.concat(";").concat(transactionValue.toString()).concat("\n");
        return csvString;
    }
}
